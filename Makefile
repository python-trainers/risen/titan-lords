.PHONY: build

run:
	uv run python src/main.py

lint:
	uv run ruff check

lintfix: format
	uv run ruff check --fix

format:
	uv run ruff format

build:
	rm -rf build
	rm -rf dist
	uv run pyinstaller \
		-Fwn titanlords \
		-p src \
		src/main.py

	cp trainerbase.standalone.toml dist/trainerbase.toml
	cp -r .venv/Lib/site-packages/trainerbase_vendor/ dist/
	rm -f titanlords.spec
