from trainerbase.gameobject import GameDouble, GameUnsignedInt
from trainerbase.memory import Address

from memory import (
    last_used_item_count_pointer,
    new_enemy_hp_pointer_for_one_hit_kill,
    player_coords_struct_address,
    player_stats_base_address,
)


health = GameUnsignedInt(player_stats_base_address + [0x5D0])
glory = GameUnsignedInt(player_stats_base_address + [0x5D4])
soul = GameUnsignedInt(player_stats_base_address + [0x5D8])

melee = GameUnsignedInt(player_stats_base_address + [0x5AC])
ranged = GameUnsignedInt(player_stats_base_address + [0x5B0])
cunning = GameUnsignedInt(player_stats_base_address + [0x5B4])
influence = GameUnsignedInt(player_stats_base_address + [0x5B8])
toughness = GameUnsignedInt(player_stats_base_address + [0x5BC])
dexterity = GameUnsignedInt(player_stats_base_address + [0x5C0])
magic = GameUnsignedInt(player_stats_base_address + [0x5C4])
spirit = GameUnsignedInt(player_stats_base_address + [0x5C8])

player_x = GameDouble(player_coords_struct_address)
player_z = GameDouble(player_coords_struct_address + 0x8)
player_y = GameDouble(player_coords_struct_address + 0x10)

last_used_item_count = GameUnsignedInt(Address(last_used_item_count_pointer, [0x30]))

new_enemy_hp_for_one_hit_kill = GameUnsignedInt(new_enemy_hp_pointer_for_one_hit_kill)
