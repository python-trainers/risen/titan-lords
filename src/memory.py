from trainerbase.memory import Address, allocate
from trainerbase.process import pm


last_used_item_count_pointer = allocate()
new_enemy_hp_pointer_for_one_hit_kill = allocate()

player_base_address = Address(pm.base_address + 0x1003618, [0x10, 0x18, 0x0])
player_stats_base_address = player_base_address + [0x38, 0x10]
player_coords_struct_address = player_base_address + [0x140, 0x248, 0x8, 0x1D8]
