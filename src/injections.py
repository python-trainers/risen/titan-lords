from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection, MultipleCodeInjection
from trainerbase.process import pm

from memory import last_used_item_count_pointer, new_enemy_hp_pointer_for_one_hit_kill


update_last_used_item_count_pointer = AllocatingCodeInjection(
    pm.base_address + 0x359534,
    f"""
        cmovg eax, r9d
        add rcx, 0x8

        mov rbp, {last_used_item_count_pointer}
        mov [rbp], rcx

        mov rbp, r8
        mov [rcx + 0x30], eax
    """,
    original_code_length=14,
)

infinite_fly_energy = CodeInjection(pm.base_address + 0x328884, "nop\n" * 8)

one_hit_kill_for_hero = AllocatingCodeInjection(
    pm.base_address + 0x2D8769,
    f"""
        push rax
        mov rax, {new_enemy_hp_pointer_for_one_hit_kill}
        mov esi, [rax]
        pop rax

        mov [rbx + rcx * 0x4 + 0x20], esi
        mov rax, [rbx]
        mov edx, edi
        mov rcx, rbx
    """,
    original_code_length=12,
)

one_hit_kill_for_ship = AllocatingCodeInjection(
    pm.base_address + 0x6DC2A8,
    f"""
        push rbx
        mov rbx, {new_enemy_hp_pointer_for_one_hit_kill}

        xor r8d, r8d

        mov eax, [rbx]

        pop rbx

        mov [rcx + 0x67C], eax
        xor ecx, ecx
    """,
    original_code_length=11,
)

one_hit_kill = MultipleCodeInjection(one_hit_kill_for_hero, one_hit_kill_for_ship)
