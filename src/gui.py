from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI

from injections import infinite_fly_energy, one_hit_kill
from objects import (
    cunning,
    dexterity,
    glory,
    health,
    influence,
    last_used_item_count,
    magic,
    melee,
    new_enemy_hp_for_one_hit_kill,
    ranged,
    soul,
    spirit,
    toughness,
)
from speedhack import speedhack
from teleport import tp


@simple_trainerbase_menu("Risen 3: Titan Lords", 730, 400)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(health, "Health", "Shift+F1", "F1", default_setter_input_value=100_000),
                GameObjectUI(glory, "Glory"),
                GameObjectUI(soul, "Soul"),
                GameObjectUI(last_used_item_count, "Last Used Item Count"),
                SeparatorUI(),
                CodeInjectionUI(infinite_fly_energy, "Infinite Fly Energy", "F2"),
                CodeInjectionUI(one_hit_kill, "One Hit Kill", "F3"),
            )

        with dpg.tab(label="Attributes"):
            add_components(
                GameObjectUI(melee, "Melee", default_setter_input_value=100),
                GameObjectUI(ranged, "Ranged", default_setter_input_value=100),
                GameObjectUI(cunning, "Cunning", default_setter_input_value=100),
                GameObjectUI(influence, "Influence", default_setter_input_value=100),
                GameObjectUI(toughness, "Toughness", default_setter_input_value=100),
                GameObjectUI(dexterity, "Dexterity", default_setter_input_value=100),
                GameObjectUI(magic, "Magic", default_setter_input_value=100),
                GameObjectUI(spirit, "Spirit", default_setter_input_value=100),
            )

        with dpg.tab(label="Teleport & Speedhack"):
            add_components(
                TeleportUI(tp, "Insert", "Home", "End"),
                SeparatorUI(),
                SpeedHackUI(speedhack, "Alt"),
            )

        with dpg.tab(label="Settings"):
            add_components(GameObjectUI(new_enemy_hp_for_one_hit_kill, "New Enemy HP (*)"))
            dpg.add_text(
                '* For One Hit Kill. Useful for fixing a bug in "Sea Battle Against Crow".'
                "\nSet to a non-zero value, save and load to fix this bug."
            )
